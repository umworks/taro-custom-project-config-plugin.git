const pathUtil = require('path');
const fs = require('fs');
const isPlainObject = require('is-plain-object');

const DEVEL = 'development';
const DEVEL_SHORT = 'dev';
const PROD = 'production';
const PROD_SHORT = 'prod';
const TEST = 'test';

const loadJsonFile = (path) => {
	if (fs.existsSync(path)) {
		try {
			const buffer = fs.readFileSync(path);
			const json = JSON.parse(buffer.toString('utf-8'));
			if (!isPlainObject(json)) return false;
			return json;
		} catch (e) {
			return false;
		}
	}
	return false;
};

const getModeShortName = (mode) => {
	if (mode === DEVEL) return DEVEL_SHORT;
	else if (mode === PROD) return PROD_SHORT;
	return TEST;
};

class TaroCustomProjectConfigPlugin {

	constructor(ctx, options) {
		this.ctx = ctx;
		this.options = options;
		this._isLoadConfig = false;
		this._loadConfigFile = '';
		this._configData = false;

		this._isChangeConfig = false;
	}

	get taroEnv() {
		return process.env.TARO_ENV;
	}

	get customEnv() {
		return process.env.CUSTOM_ENV || '';
	}

	get mode() {
		switch ((process.env.mode + '').toLowerCase()) {
			case 'test' :
			case 'uat' :
			case 't' :
				return TEST;
			case 'development' :
			case 'devel' :
			case 'dev' :
			case 'd' :
				return DEVEL;
			case 'production' :
			case 'prod' :
			case 'p':
				return PROD;
			default :
				return process.env.NODE_ENV || PROD;
		}
	}

	get shouldGenerateConfigFile() {
		return ['weapp', 'tt', 'swan', 'qq', 'jd'].indexOf(this.taroEnv) > -1;
	}

	get configFileName() {
		let fileName = 'project.config.json';
		if (this.taroEnv === 'swan') {
			fileName = 'project.swan.json';
		}
		return fileName;
	}

	generateConfigFiles() {
		const fileName = this.configFileName;
		const files = [];

		const mode = getModeShortName(this.mode);

		if (this.customEnv !== '') {
			files.push(fileName.replace(/\.json$/, `.${mode}.${this.customEnv}.json`));
			files.push(fileName.replace(/\.json$/, `.${this.customEnv}.json`));
		}

		files.push(fileName.replace(/\.json$/, `.${mode}.json`));
		files.push(fileName);

		return files;
	}

	sourceOf(path) {
		return pathUtil.join(this.ctx.paths.sourcePath, path);
	}

	projectConfigOf(file) {
		return this.sourceOf(this.makeProjectConfigPath(file));
	}

	makeProjectConfigPath(file) {
		return `mp/${this.taroEnv}/${file}`;
	}

	outputOf(path) {
		return pathUtil.join(this.ctx.paths.outputPath, path);
	}

	loadConfig() {
		if (this._isLoadConfig) return this;

		this._isLoadConfig = true;

		const configFile = this.generateConfigFiles()
			                   .filter(file => fs.existsSync(this.projectConfigOf(file)))[0] || false;

		if (configFile !== false) {
			const configPath = this.projectConfigOf(configFile);
			this._isChangeConfig = true;
			this._loadConfigFile = configFile;
			this._configData = loadJsonFile(configPath);

			fs.watchFile(configPath, () => {
				this._configData = loadJsonFile(configPath);
				if (this._configData === false) {
					this.log('unlink', '删除配置', `${configPath}`);
				} else {
					this._isChangeConfig = true;
					this.log('modify', '修改配置', `${configPath}`);
					this.writeProjectConfig();
				}
			});

			this.log('start', '项目配置', `${configPath}`);
		} else {
			this.log('warning', '项目配置', '未发现项目配置文件');
		}

		return this;
	}

	get configData() {
		return this._configData;
	}

	get isLoadConfig() {
		return this._isLoadConfig;
	}

	writeProjectConfig() {
		if (this.configData !== false && this._isChangeConfig) {
			this.ctx.writeFileToDist({
				filePath: this.configFileName,
				content : JSON.stringify(this.filterConfigData(this.configData), null, 2),
			});
			this.log('generate', '写入配置', `${this.outputOf(this.configFileName)}`);
		}
		return this;
	}

	filterConfigData(data) {
		const clone = Object.assign({}, data || {});
		if (!!clone.projectname) {
			if (this.mode !== PROD && clone.projectname.indexOf(`[${this.mode}]`) <= 0) {
				clone.projectname += `[${this.mode}]`;
			}
		}
		return clone;
	}

	log(type, sub, message) {
		if (!message)
			this.ctx.helper.printLog(type, '项目配置', sub);
		this.ctx.helper.printLog(type, sub, message);
	}
}

module.exports = (ctx, options) => {
	const plugin = new TaroCustomProjectConfigPlugin(ctx, options);
	// plugin 主体
	ctx.onBuildStart(() => {
		if (plugin.shouldGenerateConfigFile) {
			plugin.loadConfig();
		}
	});
	ctx.onBuildFinish(() => {
		if (plugin.shouldGenerateConfigFile) {
			plugin.writeProjectConfig();
		}
	});
};
