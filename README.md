# Taro 项目自定义配置文件

小程序的 project.config.json 文件的管理，是目前小程序开发最大的痛点，他严重限制小程序开发的想象力和生产效率。

该插件允许在 Taro 项目内存在多个小程序的 project.config 的定义，以实现一套源代码（同一个仓库源），输出不同的小程序。

强调：目前只对 微信（weapp）、快手（tt-抖音、今日头条）、百度（swan）、京东（jd）、QQ（qq） 这几个小程序平台。实际只测试过微信和快手，其他平台看情况再做后续的更新。

## 使用方法

```shell
yarn add taro-custom-project-config-plugin cross-env
```

在任意的 Taro 的启动指令，改为如下：

```shell
cross-env MODE=dev CUSTOM_ENV=username npm run build:weapp -- --watch
```

在 Taro 项目的配置文件加上插件：

```js
module.exports = {
    plugins: [
        'taro-custom-project-config-plugin',    
    ]
}
```

在任意的 Taro 项目的源代码目录（如：`src`），添加一个 `mp` 的目录，完整为：`src/mp` ，在此目录下，创建对应小程序平台名的目录，如：weapp, tt, qq, swan 等，与 Taro 的 PlatformName 对应，如：

```
src/mp/weapp/    => 小程序
src/mp/tt/       => 快手
```

在对应的目录内添加 `project.config.json` 文件（百度小程序是 `project.swan.json` ）。

配置文件是自动选择性的，根据你传入的 `MODE` （取值：dev|test|prod） `CUSTOM_ENV`（取值：任意字符串） 环境变量的值，他的优先选择顺序如下：

```
const files = [
    'project.config.MODE.CUSTOM_ENV.json',
    'project.config.CUSTOM_ENV.json',
    'project.config.MODE.json',
    'project.config.json',
];
```

## 补充说明

非 PROD 的话，小程序的名称字段，会强制拼接字符串 `[development]` 或 `[test]` ，主要是为了便于区分不同的环境，避免小程序上传错了。

假定你添加了微信的配置文件，但是没添加快手的配置文件，在编译快手的小程序时，他会按照 Taro 原有的业务顺序去操作，不会产生任何影响。

在开发模式下，当确定使用了某个项目配置文件后，会启动一个 `fs.watchFile` 自动跟踪这个配置文件，确保他修改、删除（不做任何处理，只是通知）、重新添加，都会自动重新生成。 
